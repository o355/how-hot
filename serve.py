# How Hot Is It In My Dorm Room
# Copyright (C) 2021  Owen McGinley

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from flask import Flask, request, render_template
import json
from datetime import datetime
import boto3
from configparser import ConfigParser

config = ConfigParser()
config.read("config.ini")

access_key_aws = config.read("AWS", "accesskey")
secret_key_aws = config.read("AWS", "secretkey")
region_aws = config.read("AWS", "region")
dbname_aws = config.read("AWS", "dbname")

session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name=region_aws)
db = session.resource('dynamodb')
table = db.Table(dbname_aws)

app = Flask(__name__)


def loaddata(filename):
    with open(filename) as f:
        return json.load(f)


@app.route("/", methods=['GET'])
def main_route():
    response = table.scan()
    fulldata_temp = {}
    fulldata_hum = {}
    fulldata_deltat = {}
    fulldata_deltarh = {}
    newest_time = 0
    newest_time_ds = 0
    for i in response['Items']:
        if int(i['timestamp']) > newest_time:
            newest_time = int(i['timestamp'])

        fulldata_temp[str(int(i['timestamp']))] = float(i['insidetemp'])
        fulldata_hum[str(int(i['timestamp']))] = float(i['insidehum'])

    for i in response['Items']:
        try:
            fulldata_deltat[str(int(i['timestamp']))] = float(i['deltat'])
            fulldata_deltarh[str(int(i['timestamp']))] = float(i['deltarh'])

            if int(i['timestamp']) > newest_time_ds:
                newest_time_ds = int(i['timestamp'])
        except KeyError:
            continue

    curtemp = round(fulldata_temp[str(newest_time)], 2)

    onclick_url = ""

    if curtemp < 68:
        status = "active"
        onclick_url = 'onclick=window.open("https://www.youtube.com/watch?v=rog8ou-ZepE")'
    elif curtemp < 77:
        status = "positive"
    elif curtemp < 83:
        status = "intermediary"
    else:
        status = "negative"
        onclick_url = 'onclick=window.open("https://www.youtube.com/watch?v=nYMeJSehCe4")'

    next_responsetime = newest_time + 300

    try:
        deltat_24h = round(fulldata_temp[str(newest_time)] - fulldata_temp[str(newest_time - 86400)], 2)
        deltat_24h = '{:.2f}'.format(deltat_24h)
    except KeyError:
        deltat_24h = "N/A"

    try:
        deltarh_24h = round(fulldata_hum[str(newest_time)] - fulldata_hum[str(newest_time - 86400)], 2)
        deltarh_24h = '{:.2f}'.format(deltarh_24h)

    except KeyError:
        deltarh_24h = "N/A"

    latest_dtobj = datetime.fromtimestamp(newest_time)

    return render_template("page.html",
                           curtemp='{:.2f}'.format(round(fulldata_temp[str(newest_time)], 2)),
                           curhum=round(fulldata_hum[str(newest_time)], 2),
                           lastupdated=latest_dtobj.strftime("%B %-d at %l:%M %p"),
                           timestamp_cur=newest_time,
                           fulldata_temp=fulldata_temp,
                           fulldata_hum=fulldata_hum,
                           fulldata_deltat=fulldata_deltat,
                           deltat_24h=deltat_24h,
                           deltarh_24h=deltarh_24h,
                           fulldata_deltarh=fulldata_deltarh,
                           # deltat=round(fulldata_deltat[str(newest_time_ds)], 2),
                           # deltarh=round(fulldata_deltarh[str(newest_time_ds)], 2),
                           next_responsetime=next_responsetime,
                           status=status,
                           onclick=onclick_url)


if __name__ == "__main__":
    app.run()
