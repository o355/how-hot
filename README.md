# How Hot Is It In My Dorm Room
Web portal utilizing PyWeather 2 to answer the question - How Hot Is It In My Dorm Room? (also known as How Hot and How Hot 1)

**Note: How Hot was developed significantly later than PyWeather 2 (November 2020).**

# How Hot Is It In My Dorm Room demo photos
They're available on my website!

https://owenthe.dev/how-hot-is-it-in-my-dorm-room

# Why make How Hot Is It In My Dorm Room?
How Hot was my first dabble into basically syncing data from a physical, data-grabbing project (PyWeather 2) to a software-only project (How Hot) via a central database, but I had some other reasons.

* My room was really hot on the day I made How Hot
* I wanted to know how hot my dorm room was before coming back, so I knew when to time it (or when to turn on the fans)
* I could get more precise information from the web portal versus just looking at the PyWeather 2 display
* Seeing if the room was warming up or cooling down over the past couple of days is genuinely useful information to know, especially after a period of warm or cool weather
* I had 45 minutes before class and this burning desire to make this (so a lot of the code is basically copy and pasted from WebWork Status)
* Graphs!

In just 45 minutes, I made a very bare but working version of How Hot. Over the next couple of days, I continued to refine it into a pretty nice web app.

# Pre-requisites
How Hot requires an active installation of PyWeather 2 (NOT PyWeather 3, that gets paired to How Hot 2) that is uploading to DynamoDB.

## Software Setup
On the software side, How Hot requires
* Python 3.5
* Boto3, Flask & Datetime libraries
* A web server to serve How Hot from
* As mentioned, an active PyWeather 2 install going to a DynamoDB database

# Configuration Setup
config.ini contains all the configuration files needed to get configured with AWS.

If you configured PyWeather 2 with AWS upload enabled, you can transfer all the configuration values right over to How Hot Is It In My Dorm Room.

## AWS section
`access_key` and `secret_key` are your access & secret keys that can read from the DynamoDB set up for data upload. These keys only need read access, if you prefer to use separate keys.

`region` is the region in which the DynamoDB database is being hosted (such as `us-east-2`).

`dbname` is the name of the DynamoDB database being used for data upload.

# Web Server Setup
You'll likely end up using WSGI to serve How Hot Is It In My Dorm Room. I've included a serve.wsgi file that I used on my Apache2 web server.

No special configurations or tweaks are needed on the web server side to get everything running.

## Note about relative paths
If you get internal server errors when running How Hot for the first time, you may need to edit `serve.py` so that the configuration file doesn't use a relative path, and instead use an absolute path.

Once the web server is up (and some data exists in the DynamoDB database being used for upload), you should be good to go! You can now monitor indoor environmental data generated from a PyWeather 2 instance.

# Further customization
You'll probably want to customize the title of the page, about sections, etc etc. The code here is what was running on my web server when this was up.

# License
How Hot Is It In My Dorm Room is licensed under the MIT License.

How Hot Is It In My Dorm Room includes a status button CSS library that I can't locate the original source for. As far as I remember, that was licensed under the MIT License (or something similar).
